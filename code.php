<?php

class Building {
    private $name;
    private $floors;
    private $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    // Getter for property name
    public function getName(){
        return $this->name;
    }

    // Setter for property name
    public function setName($name){
        $this->name = $name;
    }

    // Getter for property floors
    public function getFloors(){
        return $this->floors;
    }

    // Getter for property address
    public function getAddress(){
        return $this->address;
    }
}

class Condominium extends Building{
    public function __construct($name, $floors, $address){
        parent::__construct($name, $floors, $address);
    }

    // Override setter for floors (prevent changes)
    public function setFloors($floors){
        // Floors cannot be changed after creation
        // Do nothing or display a message, throw an exception, etc.
    }

    // Override setter for address (prevent changes)
    public function setAddress($address){
        // Address cannot be changed after creation
        // Do nothing or display a message, throw an exception, etc.
    }
}

$building = new Building('Casswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$building->setName('Caswynn Complex');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
$condominium->setName('Enzo Tower');

?>

